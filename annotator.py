import xml.etree.ElementTree as ET
import os
import ast
import reviewData as rd
from fuzzywuzzy import fuzz

def main():
    print('Welcome to Annotator Script3.5\nPlease Choose Your Dataset Below: ')
    cats = ['FOOD','PRICE','SERVICE','AMBIENCE']
    arr = os.listdir('hasil/')
    for i in range(len(arr)):
    	print(str(i+1) + '. ' + arr[i])
    #number = input('Dataset: ')
    corpus = ET.parse('hasil/dataset_test.xml')  
    root = corpus.getroot()
    # start = input('start at review (0-' + str(len(root)) + ') : ')
    i = 0
    with open('source/testing.txt', 'r') as fd:
        content = ast.literal_eval(fd.readlines()[0])
    output = rd.getReviewData()
    for review in root:
        try:
            print(i)
            aspects = review.find('aspects')
            if aspects is None:
                aspects = ET.Element('aspects')
                review.append(aspects)
                if content[i] == '': 
                    # try:
                        # if i < int(start):
                        #     i += 1
                        #     continue
                        # print('review '+str(i))
                        # print(review.find('text').text)
                        tulisan = review.find('text').text
                        for cat in cats:
                        #     grade = input(cat+'? [good/bad] ')
                            sum_positif = 0
                            sum_negatif = 0
                            for key in output:
                                if key.split('_')[0].upper() == cat:
                                    for j in range(len(output[key])):
                                        if key.split('_')[1] == 'positive':
                                            sum_positif += fuzz.ratio(tulisan, output[key][j])
                                        else:
                                            sum_negatif += fuzz.ratio(tulisan, output[key][j])
                        #     if grade != '':                        
                        #         if grade == 'good':
                            temp = len(tulisan.split(' ')) * 55
                            if sum_positif > temp or sum_negatif > temp:
                                aspect = ET.SubElement(aspects, 'aspect')
                                aspect.set('category', cat)
                                if sum_positif > sum_negatif:
                                    aspect.set('polarity', 'POSITIVE')
                                else:
                                    aspect.set('polarity', 'NEGATIVE')
                        #             break
                        #     else:
                        #         break
                                # i += 1
                                # continue
                            # break
                else:
                    tulisan = review.find('text').text.split(' ')
                    aspek = content[i].split(' ')
                    for l in range(len(aspek)):
                        max_positif = 0
                        max_negatif = 0
                        # for k in range(len(tulisan)):
                        positif = aspek[l].lower() + '_positive'
                        negatif = aspek[l].lower() + '_negative'
                        for m in range(len(output[positif])):
                            if fuzz.ratio(output[positif][m], tulisan) > max_positif:
                                max_positif = fuzz.ratio(output[positif][m], tulisan)
                        for m in range(len(output[negatif])):
                            if fuzz.ratio(output[negatif][m], tulisan) > max_negatif:
                                max_negatif = fuzz.ratio(output[negatif][m], tulisan)
                        if max_positif != max_negatif:
                            aspect = ET.SubElement(aspects, 'aspect')
                            aspect.set('category', aspek[l])
                            if max_positif > max_negatif:
                                aspect.set('polarity', 'POSITIVE')
                            else:
                                aspect.set('polarity', 'NEGATIVE')
                i += 1
            # else:
                # if len(aspects) == 0:
                #     try:
                #         print('review '+str(i))
                #         print(review.find('text').text)
                #         for cat in cats:
                #             grade = input(cat+'? [good/bad] ')
                #             aspect = ET.SubElement(aspects, 'aspect')
                #             aspect.set('category', cat)
                #             if grade != '':                        
                #                 if grade == 'good':
                #                     aspect.set('polarity', 'POSITIVE')
                #                 elif grade == 'bad':
                #                     aspect.set('polarity', 'NEGATIVE')
                #                 else:
                #                     break
                #             else:
                #                 break
                #     except (KeyboardInterrupt, TypeError) as e:
                #         print('\nThank You for Using Annotator Script3.5')
                #         break
                # i += 1
                # continue
                # for j in range(len(aspects)):
                #     aspects.remove(aspects[0])
        # except TypeError:
        except (KeyboardInterrupt, TypeError) as e:
            print('\nThank You for Using Annotator Script3.5')
            break
    corpus.write('hasil/' + arr[int(number) - 1])

if __name__ == '__main__':
	main()
