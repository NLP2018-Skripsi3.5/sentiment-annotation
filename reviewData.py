import re
import csv
import collections
import nltk
import xml.etree.ElementTree as ET

"""docstring for twitterClean"""
def __init__(self):
    super(reviewData, self).__init__()

def removeSymbol(corpus):
    _new = []      
    for _temp in corpus:
        _temp =  re.sub( r'[^A-Za-z\ ]','',_temp)
        _new.append(_temp)

    return _new

def renameUser(corpus):
    _new = []      
    for _temp in corpus:
        _temp =  re.sub( r'(^|[^@\w])@(\w{1,15})\b','',_temp)
        _new.append(_temp)

    return _new

def removeHashtag(corpus):
    _new = []
    for _temp in corpus:
        _temp = re.sub(r'#(\w+)', '', _temp)
        _new.append(_temp)

    return _new

def removeURL(corpus):
    _new = []
    for _temp in corpus:
        _temp = re.sub(r'http:\S+', '', _temp, flags=re.MULTILINE)
        _temp = re.sub(r'https:\S+', '', _temp, flags=re.MULTILINE)
        _new.append(_temp)

    return _new

def removeEmoticon(corpus):
    _new = []
    emoticons_str = r"(?:[:=;B\-][oO\"\_\-]?[\-D\)\]\(\]/\\Op3]{2,3})"
    for _temp in corpus:
        _temp.replace(emoticons_str, '')
        _temp = re.sub(r'[^\x00-\x7F]', '', _temp)
        _new.append(_temp)

    return _new
    
def getKeySentences(text,keyword):
    hasil = []
    sentences = text.split('.')
    for kalimat in sentences:
        katakata = kalimat.split(' ')
        for word in keyword:
            for i in range(len(katakata)):
                kata = katakata[i]
                if word in kata:
                    for j in range(5):
                        if (i-2+j >= 0 and i-2+j < len(katakata)):
                            hasil.append(katakata[i-2+j])
                    break
            else:
                continue
            break
    return hasil
    
def countWord(corpus):
    i = 0
    wordDictionary = {}
    countToken = 0
    for token in corpus:
        if len(token) > 2:
            countToken+=1
            if token in wordDictionary:
                wordDictionary[token] += 1
            else:
                wordDictionary[token] = 1
    return wordDictionary, countToken

def getReviewData():
    #Gain large corpus of tweets
    output = {}
    category = ['FOOD', 'PRICE', 'SERVICE', 'AMBIENCE']
    polarity = ['POSITIVE', 'NEGATIVE']
    food = ['food', 'makanan', 'makanannya', 'minuman', 'makan', 'minum', 'menu', 'taste', 'nasi', 'chicken', 'coffee', 'ayam', 'tea', 'mie', 'rice', 'cheese', 'sauce', 'beef', 'ramen', 'kopi', 'porsinya', 'porsi', 'daging', 'cake', 'dessert', 'pizza', 'chocolate', 'pork', 'steak', 'bakmi', 'eat', 'salmon', 'martabak', 'pasta', 'roti', 'burger', 'portion', 'latte', 'bakso', 'ikan', 'dinner', 'dagingnya', 'sambal', 'curry', 'fish', 'soto', 'goreng', 'bakmi', 'bumbu', 'seafood', 'menunya', 'tasted', 'salad']
    amb = ['place', 'suasana', 'tempat', 'tempatnya', 'ambience', 'nongkrong', 'asik', 'nyaman']
    serv = ['pelayanan', 'pelayanannya', 'pelayan', 'pelayannya', 'service', 'ordered', 'order', 'jaga', 'ramah', 'baik', 'friendly']
    price = ['harga', 'harganya', 'price', 'cost', 'deal', 'murah', 'mahal', 'cheap', 'expensive']
    outlier = ['the', 'dan', 'and', 'yang', 'was', 'nya', 'for', 'ada', 'ini','untuk']
    for i in range(len(category)):
        for j in range(len(polarity)):
            output[category[i].lower() + '_' + polarity[j].lower()] = []
    corpus = ET.parse('dataset/training_set.xml')
    root = corpus.getroot()
    for review in root:
        subelem = review.find('aspects')
        aspects = subelem.findall('aspect')
        for aspect in aspects:
            for key in output:
                tmp = key.split('_')
                if aspect.attrib['category'] == tmp[0].upper():
                    if aspect.attrib['polarity'] == tmp[1].upper():
                        revText = review.find('text').text
                        keySentences = getKeySentences(revText,tmp[0])
                        output[key] += keySentences
    for key in output:
        _temp = removeURL(output[key])
        _temp = renameUser(_temp)
        _temp = removeHashtag(_temp)
        _temp = removeEmoticon(_temp)
        _temp = removeSymbol(_temp)
        output[key] = []
        for sentences in _temp:
            token = nltk.wordpunct_tokenize(sentences.lower())
            output[key] += token
        unigramTable, lenAll = countWord(output[key])
        sortedTable = sorted(unigramTable.items(), key=lambda x:x[1], reverse=True)
        #print(len(unigramTable))
        top = 0
        output[key] = []
        for kata in sortedTable:
            #if top < 3:
            #    top = top + 1
            #    continue
            if kata[0] in outlier:
                top = top + 1
                continue
            if top > 251:
                break
            output[key].append(kata[0])
            top = top + 1
        #output[key] = [item for item, count in collections.Counter(output[key]).items() if count > 1]
        with open('source/' + key +'.txt', 'w') as fd:
            fd.write(str(output[key]))

    return output

if __name__ == '__main__':
	getReviewData()
