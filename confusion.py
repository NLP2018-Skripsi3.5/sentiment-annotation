import re
import csv
import json
import collections
import nltk
from nltk.corpus import stopwords
import xml.etree.ElementTree as ET
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory

TESTING_DATA_SET = 'dataset/dataset_test.xml'
TRAINING_DATA_SET = 'dataset/training_set.xml'
VALIDATION_DATA_SET = 'dataset/validation_set.xml'
GOLD = 'dataset/gold_submission.xml'

CATEGORY_WORDS = 'source/CATEGORY2.txt'
POLARITY_WORDS = 'source/POLARITY2.txt'

TESTING_SEQUENCE_RESULT = 'hasil/testing_result_sequence.txt'
TRAINING_SEQUENCE_RESULT = 'hasil/training_result_sequence.txt'
VALIDATION_SEQUENCE_RESULT = 'hasil/validation_result_sequence.txt'



def removeSymbol(corpus):
    _new = []      
    for _temp in corpus:
        _temp =  re.sub( r'[^A-Za-z\ ]','',_temp)
        _new.append(_temp)

    return _new

def renameUser(corpus):
    _new = []      
    for _temp in corpus:
        _temp =  re.sub( r'(^|[^@\w])@(\w{1,15})\b','',_temp)
        _new.append(_temp)

    return _new

def removeHashtag(corpus):
    _new = []
    for _temp in corpus:
        _temp = re.sub(r'#(\w+)', '', _temp)
        _new.append(_temp)

    return _new

def removeURL(corpus):
    _new = []
    for _temp in corpus:
        _temp = re.sub(r'http:\S+', '', _temp, flags=re.MULTILINE)
        _temp = re.sub(r'https:\S+', '', _temp, flags=re.MULTILINE)
        _new.append(_temp)

    return _new

def removeEmoticon(corpus):
    _new = []
    emoticons_str = r"(?:[:=;B\-][oO\"\_\-]?[\-D\)\]\(\]/\\Op3]{2,3})"
    for _temp in corpus:
        _temp.replace(emoticons_str, ' ')
        _temp = re.sub(r'[^\x00-\x7F]', ' ', _temp)
        _new.append(_temp)

    return _new


def createKeyWords():
	file = open(CATEGORY_WORDS, 'r') 

	FOOD = set([])
	PRICE = set([])
	SERVICE = set([])
	AMBIENCE = set([])

	for line in file:
		arr = line.replace('\n','')
		arr = arr.split(' ')

		if arr[1] == 'FOOD':
			FOOD.add(arr[0])

		if arr[1] == 'PRICE':
			PRICE.add(arr[0])

		if arr[1] == 'SERVICE':
			SERVICE.add(arr[0])

		if arr[1] == 'AMBIENCE':
			AMBIENCE.add(arr[0])

	file.close()
	return FOOD, PRICE, SERVICE, AMBIENCE


def createPolarityWord():
	file = open(POLARITY_WORDS, 'r')

	POSITIVE = set([])
	NEGATIVE = set([])

	for line in file:
		arr = line.replace('\n','')
		arr = arr.split(' ')
		if arr[1] == 'POSITIVE':
			POSITIVE.add(arr[0])

		if arr[1] == 'NEGATIVE':
			NEGATIVE.add(arr[0])

	file.close()
	return POSITIVE, NEGATIVE


def sequenceLabel(dataset):
    f, p, s, a = createKeyWords()
    pos, neg = createPolarityWord()
    corpus = ET.parse(dataset)
    toFeed = []
    root = corpus.getroot()
    rawSentence = []
    for review in root:
        rawSentence.append(review.find('text').text)

    _temp = removeURL(rawSentence)
    _temp = renameUser(_temp)
    _temp = removeHashtag(_temp)
    _temp = removeEmoticon(_temp)
    _temp = removeSymbol(_temp)


    stop_wordsNeg = set(stopwords.words('negation'))
    stop_wordsEn = set(stopwords.words('english')) - stop_wordsNeg
    factory = StopWordRemoverFactory()
    stemFactory = StemmerFactory()
    stemmer = stemFactory.create_stemmer();

    stop_wordsId = set(factory.get_stop_words()) - stop_wordsNeg - set(['p', 'nya', 'yg', 'sih', 'bgt', 'jadi', 'sama', 'yaa'])

    i = 0;
    for sentences in _temp:
        #print('sentences ke-'+ str(i))
        i+=1
        token = nltk.wordpunct_tokenize(sentences.lower())
        toFeed.append(token)

    for vec in toFeed:
    	for i in range(len(vec)):
    		if vec[i] in f:
    			vec[i] = 'FOOD'
    			
    		elif vec[i] in p:
    			vec[i] = 'PRICE' 
    		
    		elif vec[i] in s:
    			vec[i] = 'SERVICE' 
    		
    		elif vec[i] in a:
    			vec[i] = 'AMBIENCE'
    		else:
    			vec[i] = 'OTHER'

    return toFeed

def sequenceToFile(sequenceLabel, output):
	file = open(output, "w") 
	for vec in sequenceLabel:
		res = ''
		for label in vec:
			if label != "OTHER":
				res+= label + ' '

		res += '\n'
		file.write(res)

	file.close()

def getTargetAspects(dataset, typeTarget):
    corpus = ET.parse(dataset)  
    root = corpus.getroot()
    target = []
    findParameter = ''
    if typeTarget == 'training':
    	findParameter = './aspects[@id="0"]'

    if typeTarget == 'validation':
    	findParameter = './aspects'

    for review in root:
        revAttrib = review.attrib
        rid = revAttrib['rid']
        for element in review.findall(findParameter):
            aspects = []
            aspects.append(rid)
            for aspect in element:
                temp = aspect.attrib
                if temp['category'] not in aspects:
                    aspects.append(temp['category'])
            target.append(aspects)

    return target

def getPredictionAspects(predictFile):
	file = open(predictFile, "r")
	cat = ['FOOD', 'PRICE', 'SERVICE', 'AMBIENCE']
	pol = ['POSITIVE', 'NEGATIVE']
	pred = []
	k = 0
	for line in file:
		aspects = []
		aspects.append(k)
		arr = line.replace('\n','')
		arr = arr.split(' ')
		for token in arr:
			if token not in aspects and token is not '':
				aspects.append(token)
		pred.append(aspects)
		k += 1

	return pred

def convertResultToFile(resultList, typeList):
	file = ''
	if typeList == 'predict':
	    file = open("hasil/validationFinalFix.txt", "w")
	if typeList == 'target':
		file = open("hasil/listofTarg.txt", "w")
	
	for i in resultList:
		file.write(str(i)) 
	file.close()


def training():
	sequence = sequenceLabel(TRAINING_DATA_SET)
	target = getTargetAspects(TRAINING_DATA_SET, 'training')

	sequenceToFile(sequence, TRAINING_SEQUENCE_RESULT)
	pred = getPredictionAspects(TRAINING_SEQUENCE_RESULT)

	fail = 0
	suc = 0
	countAspsectTarget = 0
	countAspsectPred = 0
	for i in range(len(target)):
		currentTarget = set(target[i]) 
		currentPred   = set(pred[i])
		countAspsectTarget += len(currentTarget)
		countAspsectPred += len(currentPred) 
		fail += len(currentTarget - currentPred)
		fail += len(currentPred - currentTarget)
		suc += len(currentTarget & currentPred)

	print("Jumlah aspect pada target: " + str(countAspsectTarget))
	print("Jumlah aspect pada pred: " + str(countAspsectPred))
	
	print("Total aspect: " + str(countAspsectPred + countAspsectTarget))
	print("Jumlah aspect yang gagal dilakukan labeling: " + str(fail))
	print("Jumlah aspect yang berhasil dilakukan labeling: " + str(suc))
	print("Total aspect: "+ str(fail + suc))

	print("Akurasi: " + str(suc/(fail+suc)))




def validation():
	sequence = sequenceLabel(TESTING_DATA_SET)
	#target = getTargetAspects(VALIDATION_DATA_SET, 'validation')

	sequenceToFile(sequence, TESTING_SEQUENCE_RESULT)
	pred = getPredictionAspects(TESTING_SEQUENCE_RESULT)
	convertResultToFile(pred, 'predict')
	fail = 0
	suc = 0
	countAspsectTarget = 0
	countAspsectPred = 0
	for i in range(len(target)):
		currentTarget = set(target[i]) 
		currentPred   = set(pred[i])
		countAspsectTarget += len(currentTarget)
		countAspsectPred += len(currentPred) 
		fail += len(currentTarget - currentPred)
		fail += len(currentPred - currentTarget)
		suc += len(currentTarget & currentPred)

	print("Jumlah aspect pada target: " + str(countAspsectTarget))
	print("Jumlah aspect pada pred: " + str(countAspsectPred))
	
	print("Total aspect: " + str(countAspsectPred + countAspsectTarget))
	print("Jumlah aspect yang gagal dilakukan labeling: " + str(fail))
	print("Jumlah aspect yang berhasil dilakukan labeling: " + str(suc))
	print("Total aspect: "+ str(fail + suc))

	print("Akurasi: " + str(suc/(fail+suc)))



def testing():
	sequence = sequenceLabel(TESTING_DATA_SET)
	sequenceToFile(sequence, TESTING_SEQUENCE_RESULT)
	pred = getPredictionAspects(TESTING_SEQUENCE_RESULT)
	convertResultToFile(pred, 'predict')
	target = getTargetAspects(GOLD, 'validation')
	print(len(target))
	FP = 0
	FN = 0
	fail = 0
	suc = 0
	countAspsectTarget = 0
	countAspsectPred = 0
	for i in range(len(pred)):
		for j in range(len(target)):
			if(str(target[j][0]) == str(pred[i][0])):
				rid = set([target[j][0]])
				rid2 = set([pred[i][0]])
				currentTarget = set(target[j]) - rid
				currentPred   = set(pred[i]) - rid2
				countAspsectTarget += len(currentTarget)
				countAspsectPred += len(currentPred) 
				fail += len(currentTarget - currentPred)
				fail += len(currentPred - currentTarget)
				suc += len(currentTarget & currentPred)

				FP += len(currentPred - currentTarget)
				FN += len(currentTarget - currentPred)

	print("Jumlah aspect pada target: " + str(countAspsectTarget))
	print("Jumlah aspect pada pred: " + str(countAspsectPred))
	
	print("Total aspect: " + str(countAspsectPred + countAspsectTarget))
	print("Jumlah aspect yang gagal dilakukan labeling: " + str(fail))
	print("Jumlah aspect yang berhasil dilakukan labeling: " + str(suc))
	print("Total aspect: "+ str(fail + suc))

	print("TP: " + str(suc))
	print("FN: " + str(FN))
	print("FP: " + str(FP))

	print("Akurasi: " + str(suc/(fail+suc)))
	TP = suc
	print("Precision: " + str(TP/(TP+FP)))
	print("Recall: " + str(TP/(TP+FN)))
	print("F1: " + str(2*TP/(2*TP+FP+FN)))
	
testing()