# import xml.etree.ElementTree
from xml.dom import minidom
# from yattag import indent
# from dicttoxml import dicttoxml
import xmltodict
import os

def main():
	arr = os.listdir('hasil/')
	for i in range(len(arr)):
		temp = 'hasil/' + arr[i]
		print(temp)
		if '.xml' in temp:
			with open(temp) as fd:
				fd = fd.read()
				data = xmltodict.parse(fd)
			# data = xml.etree.ElementTree.parse(temp).getroot()
			# data = minidom.parse(temp)
			for j in range(len(data['corpus']['review'])):
				print(data['corpus']['review'][j])
			# xml = dicttoxml(data, attr_type=False)
			with open(temp, 'w') as fd:
				data = xmltodict.unparse(data)
				data = minidom.parseString(data).toprettyxml()
				fd.write(data)
				fd.close()

if __name__ == '__main__':
	main()
