import reviewData
import operator
from itertools import islice

def countWord():
	arr = reviewData.getReviewData()
	i = 0
	wordDictionary = {}
	countToken = 0
	for key in arr:
		# for review in corpus:
		for token in arr[key]:
			countToken+=1
			if token in wordDictionary:
				wordDictionary[token] += 1
			else:
				wordDictionary[token] = 1
	return wordDictionary, countToken

def take(n, iterable):
    "Return first n items of the iterable as a list"
    return list(islice(iterable, n))

def main():
	unigramTable, lenAll = countWord()
	sortedTable = sorted(unigramTable.items(), key=lambda x:x[1], reverse=True)
	print(len(unigramTable))
	print(sortedTable)

if __name__ == '__main__':
	main()