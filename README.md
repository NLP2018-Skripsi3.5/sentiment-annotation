# Sentiment Annotation Natural Language Processing

## Restaurant Review Sentiment Analysis

## How the Applications Work

The application is console-based which can be executed from the terminal. The general steps are as follows:

1. The program will import the data from the .xml that provided. 
2. The review will be analyze their sentiment (positive / negative)

More detailed information can be found on the program source code or by clicking [here](https://drive.google.com/open?id=1nm68_6mX8v1Q2lWffSe3heRsfeaEep3G).

Main Focus:
* Parse Review Data & Print Output
* Analyze sentiment (Positive / Negative). 

## Getting Started (How to Run the Program)

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites (How to set up your machine)

1. Navigate to the directory where is the root of this project folder.
2. Still in the directory where is the root of this project, install all its dependencies.

    ```bash
    pip install -r requirements.txt
    ```

    Dependencies are all listed in `requirements.txt`. To re-generate
    this file (after you've installed new packages), simply you can
    it directly on the file. If you have a problem installing the dependencies, 
    please install java system package first.

## How the Applications Work

The application is console-based which can be executed from the terminal. The general steps are as follows:

1. Use annotator.py to annotate using Fuzzy e.g python3 annotator.py 

   **annotator2.py using different approach (Key Sentence)

2. app.py is used to rewrite any XML (Template)

3. Feature Extraction

   **Aspect Only using unigram.py

   **Aspect Sentiment using aspectLabelling.py

4. jupyter.ipynb is used to test directly using console

5. reviewData.py is used to cleansing data

6. confusion.py is used to check the accuracy between goal standard and prediction

## Built With

* [Python](https://www.python.org/) - Python 3.5.2

## Authors

* **Adityo Anggraito** - *Computer Science Student University of Indonesia* - [GitLab](https://gitlab.com/primetime49)
* **Bryanza Novirahman** - *Computer Science Student University of Indonesia* - [GitLab](https://gitlab.com/bryanzanr)
* **Damar Fajar Tanjung** - *Computer Science Student University of Indonesia* - [GitLab](https://gitlab.com/damartanjung)

## Important links
* [Google Drive](https://drive.google.com/open?id=1nm68_6mX8v1Q2lWffSe3heRsfeaEep3G)
Language Used : Python 3.5.2 (https://www.python.org/)