import xml.etree.ElementTree as ET
import os
import ast
import reviewData as rd
from fuzzywuzzy import fuzz

def main():
    print('Welcome to Annotator Script3.5\nPlease Choose Your Dataset Below: ')
    cats = ['FOOD','PRICE','SERVICE','AMBIENCE']
    arr = os.listdir('hasil/')
    for i in range(len(arr)):
        print(str(i+1) + '. ' + arr[i])
    #number = input('Dataset: ')
    corpus = ET.parse('hasil/dataset_test.xml')  
    root = corpus.getroot()
    # start = input('start at review (0-' + str(len(root)) + ') : ')
    i = 0
    with open('source/testing.txt', 'r') as fd:
        content = ast.literal_eval(fd.readlines()[0])
    output = rd.getReviewData()
    for review in root:
        print(i)
        # try:
        aspects = review.find('aspects')
        if aspects is None:
            aspects = ET.Element('aspects')
            review.append(aspects) 
            if content[i] != '':
                tulisan = review.find('text').text
                aspek = content[i].split(' ')
                for l in range(len(aspek)):
                    max_positif = 0
                    max_negatif = 0
                    aspect = ET.SubElement(aspects, 'aspect')
                    aspect.set('category', aspek[l])
                    positif = aspek[l].lower() + '_positive'
                    negatif = aspek[l].lower() + '_negative'
                    for m in range(len(output[positif])):
                        if output[positif][m] in tulisan:
                            max_positif = max_positif + 1
                    for m in range(len(output[negatif])):
                        if output[negatif][m] in tulisan:
                            max_negatif = max_negatif + 1
                    if aspek[l].lower() == 'price' and 'murah' in tulisan:
                        max_positif = max_positif+10
                    if aspek[l].lower() == 'price' and 'mahal' in tulisan:
                        max_negatif = max_negatif+10
                    if max_positif+2 > max_negatif:
                        aspect.set('polarity', 'POSITIVE')
                    else:
                        aspect.set('polarity', 'NEGATIVE')
            i += 1
    corpus.write('hasil/hasil.xml')  

if __name__ == '__main__':
    main()
