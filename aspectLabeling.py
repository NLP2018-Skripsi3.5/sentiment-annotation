import re
import csv
import collections
import nltk
from nltk.corpus import stopwords
import xml.etree.ElementTree as ET


def removeSymbol(corpus):
    _new = []      
    for _temp in corpus:
        _temp =  re.sub( r'[^A-Za-z\ ]','',_temp)
        _new.append(_temp)

    return _new

def renameUser(corpus):
    _new = []      
    for _temp in corpus:
        _temp =  re.sub( r'(^|[^@\w])@(\w{1,15})\b','',_temp)
        _new.append(_temp)

    return _new

def removeHashtag(corpus):
    _new = []
    for _temp in corpus:
        _temp = re.sub(r'#(\w+)', '', _temp)
        _new.append(_temp)

    return _new

def removeURL(corpus):
    _new = []
    for _temp in corpus:
        _temp = re.sub(r'http:\S+', '', _temp, flags=re.MULTILINE)
        _temp = re.sub(r'https:\S+', '', _temp, flags=re.MULTILINE)
        _new.append(_temp)

    return _new

def removeEmoticon(corpus):
    _new = []
    emoticons_str = r"(?:[:=;B\-][oO\"\_\-]?[\-D\)\]\(\]/\\Op3]{2,3})"
    for _temp in corpus:
        _temp.replace(emoticons_str, ' ')
        _temp = re.sub(r'[^\x00-\x7F]', ' ', _temp)
        _new.append(_temp)

    return _new


def createKeyWords():
	file = open('source/CATEGORY.txt', 'r') 

	FOOD = set([])
	PRICE = set([])
	SERVICE = set([])
	AMBIENCE = set([])

	for line in file:
		arr = line.replace('\n','')
		arr = arr.split(' ')

		if arr[1] == 'FOOD':
			FOOD.add(arr[0])

		if arr[1] == 'PRICE':
			PRICE.add(arr[0])

		if arr[1] == 'SERVICE':
			SERVICE.add(arr[0])

		if arr[1] == 'AMBIENCE':
			AMBIENCE.add(arr[0])

	file.close()
	return FOOD, PRICE, SERVICE, AMBIENCE


def sequenceLabel():
    f, p, s, a = createKeyWords()
    corpus = ET.parse('dataset/training_set.xml')
    toFeed = []
    root = corpus.getroot()
    rawSentence = []
    for review in root:
        rawSentence.append(review.find('text').text)

    _temp = removeURL(rawSentence)
    _temp = renameUser(_temp)
    _temp = removeHashtag(_temp)
    _temp = removeEmoticon(_temp)
    _temp = removeSymbol(_temp)

    for sentences in _temp:
        token = nltk.wordpunct_tokenize(sentences.lower())
        toFeed.append(token)

    for vec in toFeed:
    	for i in range(len(vec)):
    		if vec[i] in f:
    			vec[i] = 'FOOD'
    			
    		elif vec[i] in p:
    			vec[i] = 'PRICE' 
    		
    		elif vec[i] in s:
    			vec[i] = 'SERVICE' 
    		
    		elif vec[i] in a:
    			vec[i] = 'AMBIENCE'

    		else:
    			vec[i] = 'OTHER'

    return toFeed

def sequenceToFile(sequenceLabel):
	file = open("hasil/hasilSequenceLabeling2.txt", "w") 
	n = 1
	for vec in sequenceLabel:
		#res = 'Hasil Review ke-' + str(n) + ' '
		res = ''
		for label in vec:
			if label != "OTHER":
				res+= label + ' '

		res += '\n'
		file.write(res)
		n+=1

	file.close()

def getTargetAspects():
    corpus = ET.parse('dataset/training_set.xml')  
    root = corpus.getroot()
    target = []
    for review in root:
        for element in review.findall('./aspects[@id="0"]'):
            aspects = []
            for aspect in element:
                temp = aspect.attrib
                if temp['category'] not in aspects:
                    aspects.append(temp['category'])
            target.append(aspects)
    return target

def getPredictionAspects():
	file = open("hasil/hasilSequenceLabeling2.txt", "r")
	pred = []
	for line in file:
		aspects = []
		arr = line.replace('\n','')
		arr = arr.split(' ')
		for token in arr:
			if token not in aspects and token is not '':
				aspects.append(token)
		pred.append(aspects)
	return pred

def getPredToFIle(resultList):
	file = open("hasil/listofPred.txt", "w")
	for i in resultList:
		file.write(str(i)) 
	file.close()

def getTargToFIle(resultList):
	file = open("hasil/listofTarg.txt", "w")
	for i in resultList:
		file.write(str(i)) 
	file.close()

def validation():
	target = getTargetAspects()
	pred = getPredictionAspects()
	getPredToFIle(pred)
	getTargToFIle(target)
	fail = 0
	suc = 0
	countAspsectTarget = 0
	countAspsectPred = 0
	for i in range(len(target)):
		currentTarget = set(target[i]) 
		currentPred   = set(pred[i])
		countAspsectTarget += len(currentTarget)
		countAspsectPred += len(currentPred) 
		fail += len(currentTarget - currentPred)
		fail += len(currentPred - currentTarget)
		suc += len(currentTarget & currentPred)

	print("Jumlah aspect pada target: "+ str(countAspsectTarget))
	print("Jumlah aspect pada pred: "+ str(countAspsectPred))
	print("Total aspect: " +str(countAspsectPred+countAspsectTarget))
	print("Jumlah aspect yang gagal dilakukan labeling: " + str(fail))
	print("Jumlah aspect yang berhasil dilakukan labeling: " + str(suc))
	print("Total aspect: "+str(fail + suc))
	print("Akurasi: " + str(suc/(fail+suc)))



validation()
